package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/commentreaction")
public class CommentReactionController {
    @Autowired
    private CommentReactionService commentsService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponseCommentReactionDto> setReaction(@RequestBody ReceivedCommentReactionDto commentReaction){
        commentReaction.setUserId(getUserId());
        var reaction = commentsService.setReaction(commentReaction);

        if (reaction.isPresent() && !reaction.get().getUserId().equals(getUserId())) {
            if (reaction.get().getIsLike()) {
                // notify a user if someone (not himself) liked his comment
                template.convertAndSend("/topic/commentlike", "Your comment was liked!");
            }
            if (reaction.get().getIsDislike()) {
                template.convertAndSend("/topic/commentdislike", "Your comment was disliked!");
            }
        }
        return reaction;
    }
}
