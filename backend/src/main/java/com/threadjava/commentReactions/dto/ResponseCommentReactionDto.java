package com.threadjava.commentReactions.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ResponseCommentReactionDto {
    private UUID id;
    private UUID commentId;
    private UUID postId;
    private Boolean isLike;
    private Boolean isDislike;
    private UUID userId;
}
